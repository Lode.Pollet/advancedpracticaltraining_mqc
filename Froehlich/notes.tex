\input{preamble.tex}

% Uebungsinformation.
\newcommand{\semester}{Summer term 2021}
\newcommand{\authors}{Rajah Nutakki, Nico Sadoune, Lode Pollet}
\newcommand{\lva}{Advanced practical training}
\newcommand{\extype}{Diagrammatic Monte Carlo simulation of the Fr\"ohlich polaron model -- Simulation des Fr\"ohlich Modells mit diagrammatischen Monte Carlo Methoden. }
\renewcommand{\subtitle}{online April 8. Value : 2 credits}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{bm}
\usepackage{graphicx}
\usepackage{color}
\usepackage{courier}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\maketitlesection

\section*{Background and Preparation}

The background for this advanced practical training is taken from the lecture notes, J. Greitemann
and L. Pollet, Scipost Phys. Lect. Notes 2 (2018), \url{https://scipost.org/SciPostPhysLectNotes.2}. \\
Theoretical prerequisites for completion of this practical training are familiarity with the basics of 
quantum mechanics and elementary quantum field theory, statistical mechanics, and undergraduate 
computational physics including classical Monte Carlo methods.\\
For the computational tasks you will need to download and install the source codes from \\ 
\url{https://gitlab.lrz.de/Lode.Pollet/LecturesDiagrammaticMonteCarlo}.
A detailed installation guide with all requirements is provided in the '\texttt{README.md}' file.

\section*{Warm-up}

As a warm-up, we will get acquainted with continuous-time variables in perturbative expansions 
by solving a toy model consisting of a single spin coupled to 
fields in the x and z directions. The Hamiltonian for this system is Eq. 5 in the lecture notes.

\vspace*{1 cm}

\begin{task}{Tasks: Two-Level System}
\begin{itemize}
\item Read sections 1 and 2 in the lecture notes. Verify Eqs. 6 and 7.
\item Implement the algorithm described in sections 2.3 and 2.4 to calculate the spin magnetizations 
of the two-level system, whose Hamiltonian is given by Eq. 5. You may use the programming language 
of your choice and may consult the provided code when in doubt.
\item Run either your or the provided code with the parameter files 
(\texttt{two\_level\_system/params} in the git repository)
at high and low temperature. Do the results agree with the analytical results within error bars?
\item Advanced: What needs to be changed in the code to sample the partition function of the 
spin-boson model (Sec 8.5)? See in particular Ref. 50 of the lecture notes.
\end{itemize}
\end{task}


\section*{Fr\"ohlich Polaron}

The Fr\"ohlich polaron was the first system solved by diagrammatic Monte Carlo simulations. 
It describes the coupling of a single electron with longitudinal, optical phonons.
At weak coupling the electron remains a well-defined quasi-particle with a large residue. 
At strong coupling ( the Landau-Pekar regime) there is a tendency towards localization
although the quasi-particle ultimately survives with a very low residue. Diagrammatic Monte Carlo 
simulations can be performed for any value of the coupling strength $\alpha$.
The beauty of this approach is that you can actually compute higher order terms in a perturbative 
expansion, unlike for what you usually learn in text books.

\vspace*{1 cm}

\begin{task}{Tasks: Green's Function from Bare Propagators}
\begin{itemize}
\item Derive the Feynman rules for the diagrammatic expansion of the Green's function of the Fr\"ohlich polaron,
as described by equations 12 of the lecture notes.
\item Read the remainder of the lecture notes and study the code which simulates the full Green's function 
using bare propagators (\texttt{froehlich\_greenfun\_bare}).
\item Run this code at various small $\alpha = 0.5, 1, 2, 3, 4$ and extract the polaron 
residue and energy, using Eq. 21 of the lecture notes. You may need to adjust the parameter $\mu_0$ in each case. 
Compare with the predictions 
of lowest order perturbation theory. Observe how very low error bars can be obtained within minutes on a 
laptop for small $\alpha$, while for larger $\alpha$ the simulations become harder.
\item Perform simulations at $\alpha = 2$ and several (small) momenta $\mathbf{k}$
and fit the calculated $E(\mathbf{k})$ against the momenta to obtain the effective mass $m^*$ from the relation 
$E(\mathbf{k})=\frac{\mathbf{k}^2}{2m^*}$.
\item Advanced: the sampling difficulties for large $\alpha$ can be mitigated by looking at the $N-$phonon Green's functions for which it can be shown, by invoking the Lehmann representation, that they have the same energy but different residues for any $N$. Read A. Mishchenko et al., \url{https://arxiv.org/abs/cond-mat/9910025} for more information.
\end{itemize}
\end{task}

\vspace*{2cm} 
It is common practice in many-body field theories to compute the self-energy $\Sigma$ instead of the Green's function $G$. 
These two quantities are related by Dyson's equation,
%
\begin{equation}
G(\mathbf{k}, \omega_n) = \mathcal{G}_0(\mathbf{k}, \omega_n) - \Sigma(\mathbf{k}, \omega_n),
\end{equation}
%
with $\mathbf{k}$ the external momentum, $\omega_n$ the external Matsubara frequency, and $\mathcal{G}_0(\mathbf{k}, \omega_n)$ the non-interacting electron Green's function.
Diagrammatically, the self-energy corresponds to 1-particle irreducible diagrams: it contains all diagrams that do not fall apart when cutting a single electron propagator line. As such, the sampling space is reduced.
We must however still work in imaginary time, {\it i.e.}, we must Fourier transform our quantities to Matsubara frequencies before the Dyson equation can be applied.
The implications of these steps will now be examined.
\vspace*{1cm} 

\begin{task}{Tasks: Self-Energy}
\begin{itemize}
\item Study the first order term of the self-energy analytically.
\item Study how the code implements the fast Fourier transforms. Does the first order diagram need special care?
\item For $\alpha=4$ compute the self-energy with the provided code 
(\texttt{Froehlich\_Selfenergy}). 
From this quantity, obtain the Green's function by fast Fourier transform. How does your result compare to 
the previous result (\texttt{froehlich\_greenfun\_bare})?
\item How important are the 1-particle reducible diagrams that are part of the expansion for the bare Green's function (but not the self-energy) at high expansion orders? How can you answer this question from the Monte Carlo code?
\end{itemize}
\end{task}

\vspace*{2cm} 
For any absolutely convergent series, which is the case in the Fr\"ohlich polaron model as long as we keep the external $\tau_{\rm max}$ finite, we can reorder the terms in the perturbative expansion at will without changing the final answer. 
When using skeleton diagrams, one replaces bare propagators with bold ones. This further reduces the sampling space at the cost of a self-consistency loop. 
Such approaches are typically argued to take into account more many-body effects at low order than the Taylor expansion.

\vspace*{1cm} 

\begin{task}{Tasks: Bold Series (Skeleton Diagrams)}
\begin{itemize}
\item Study the non-crossing approximation (NCA) in section in section 5.1. What diagrams does NCA correspond to in the Taylor expansion (or bare series)?
\item Compute the Green's function for $\alpha=4$ in the bold scheme using the \texttt{froehlich\_selfenergy\_bold} executable 
in the provided code. How does your result compare with the result obtained by the expansion in terms of bare propagators? How close is the NCA result to the final answer?
\item Advanced: Vertex corrections can be taken into account by considering the 3-point vertex. Draw the lowest order diagrams contributing to the 3-point vertex. What would be the advantages and disadvantages of working with a full skeleton technique, {i.e.}, the full three-point vertex and the bold Green's functions are used as building blocks?
%\item analytical continuation to get the spectral function and see the processes of phonon emission and absorption???
%\item Advanced: Compute the first order Green's function at finite temperature (numerically). 
%\item Advanced: building on your previous result, compute (numerically) at finite temperature the dc-mobility of the polaron at finite temperature, ignoring vertex corrections. See e.g. David C. Langreth and Leo P. Kadanoff, Phys. Rev. {\bf 133}, A1070 (1964) for further information.
\end{itemize}
\end{task}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
