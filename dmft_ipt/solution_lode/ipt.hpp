#include <fstream>
#include <iostream>
#include <cmath>
#include <vector>
#include <complex>
#include <string>

using namespace std;


class ipt_t {
public:
  double pi;
  double t_hop;
  double U_on;
  double beta;
  int Nfreq;
  int iter;
  int max_iter;
  double omega_max;
  vector<double> omega;
  vector<double> fermi;
  int Ndos;
  vector<double> DOS_x;
  vector<double> DOS_eps;
  double hdos;
  double hfreq;
  vector<vector<complex<double> > > G0;
  vector<vector<complex<double> > > G;
  vector<double> diff;
  vector<complex<double> > Sigma;
  vector<double> Ap;
  vector<double> Am;
  vector<double> Pone;
  vector<double> Ptwo;
    
    int indx(const double w) {
        if (w <= -omega_max) return 0;
        if (w >= omega_max) return (Nfreq);
        return (static_cast<int>(( w + omega_max)/hfreq));
    }

  void init();
  complex<double> Hilbert(complex<double>);
  void compute_A();
  void compute_P();
  void compute_sigma();
  void compute_G();
  void dyson();
  void do_loop();
  void print_G();
  void print_G0();
  void read_G0();
};
