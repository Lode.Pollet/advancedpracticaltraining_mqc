#include "ipt.hpp"

int main() {
  ipt_t MyIPT;
  MyIPT.init();
    MyIPT.do_loop();
    MyIPT.print_G();
  //  for (int i=0; i < MyIPT.Ndos; i++) cout << MyIPT.DOS_x[i]/(4 * MyIPT.t_hop) << "\t" << MyIPT.DOS_eps[i] << "\n";
    MyIPT.print_G0();
}

void ipt_t::do_loop() {
  for (iter=0; iter < max_iter; iter++) {
    cout << "# Starting iteration number : " << iter << "\t";
    compute_A();
    compute_P();
    compute_sigma();
    compute_G();
    dyson();
      cout << " --  Difference : " << diff[iter] * hfreq << "\n";
  }
}

void ipt_t::print_G() {
  for (int i=0; i < Nfreq; i++) {
    cout << omega[i]/(4 * t_hop) << "\t";
    for (int j = max_iter-1; j >= 0; j--) {
      cout << -G[j][i].imag()/pi << "\t";
    }
    cout << "\n";
  }
}

void ipt_t::init() {
    pi = atan(1.)*4;
    string s;
    ifstream infile("params_ipt");
    if (infile.fail()) {
        cout << "# Error with reading file params_ipt!\n";
        char ch; cin >> ch;
    }
    infile >> t_hop; getline(infile, s);
    infile >> U_on; getline(infile, s);
    infile >> beta; getline(infile, s);
    infile >> omega_max; getline(infile, s);
    infile >> Nfreq; getline(infile, s);
    infile >> max_iter; getline(infile, s);
    if (infile.fail()) {
        cout << "# Error with reading file params_ipt!\n";
        char ch; cin >> ch;
    }
    infile.close();
    //t_hop = 0.25; // 2d -- do not change
    //U_on = 2.0;
    //beta = 20.0;
    //omega_max = 4.0;
    //Nfreq = 800;
    //max_iter = 20;
    iter = 0;
    omega.resize(Nfreq+1);
    fermi.resize(Nfreq+1);
    Ap.resize(Nfreq+1);
    Am.resize(Nfreq+1);
    Pone.resize(Nfreq+1);
    Ptwo.resize(Nfreq+1);
    G0.resize(max_iter+1);
    G.resize(max_iter+1);
    diff.resize(max_iter+1);
    for (int i=0; i < max_iter+1; i++) {
      G0[i].resize(Nfreq+1);
      G[i].resize(Nfreq+1);
      diff[i] = 0.;
    }
    Sigma.resize(Nfreq+1);
    hfreq = (2*omega_max / Nfreq);
    for (int i=0; i < Nfreq+1; i++) omega[i] = -omega_max + i * hfreq;
    for (int i=0; i < Nfreq+1; i++) fermi[i] = 1./(exp(beta * omega[i]) + 1.);
    Ndos = 100000;
    DOS_x.resize(Ndos+1);
    DOS_eps.resize(Ndos+1);
    hdos = 8*t_hop/Ndos;
    // semicircular DOS
    for (int i=0; i < Ndos+1; i++) {
        DOS_x[i] = -4*t_hop + i*hdos;
        DOS_eps[i] = sqrt( (4*t_hop)*(4*t_hop) - DOS_x[i]*DOS_x[i])/(8 * pi * t_hop * t_hop);
    }
    
    for (int i=0; i < Nfreq+1; i++) {
      //double delta = (i < Nfreq ? -0.005 : 0.005);
      double delta = 0.005;
      G0[0][i] = 1./(omega[i] + complex<double>(0., delta ));
    }
    
    //read_G0();
}

complex<double> ipt_t::Hilbert(complex<double> z) {
    complex<double> res(0.,0.);
    res += 0.5*hdos * (DOS_eps[0] / (z - DOS_x[0]) + DOS_eps[Ndos] / (z - DOS_x[Ndos]));
    for (int i=1; i < Ndos; i++) {
        res += hdos * DOS_eps[i] / (z - DOS_x[i]);
    }
    return (res);
}

void ipt_t::compute_A() {
    //cout << "# Computing A :\n";
    for (int i=0 ; i < Nfreq+1; i++) {
        Ap[i] = - G0[iter][i].imag() * fermi[i]/pi;
        Am[i] = - G0[iter][i].imag() * fermi[Nfreq-i]/pi;
        //cout << omega[i] << "\t" << Ap[i] << "\t" << Am[i] << "\n";
    }
}

void ipt_t::compute_P() { // compute polarization bubbles
    //cout << "# Computing P :\n";
    for (int i=0; i < Nfreq+1; i++) {
        Pone[i] = 0.;
        Ptwo[i] = 0.;
        for (int j=0; j < Nfreq+1; j++) {
            int k = indx(omega[j]-omega[i]);
            if ( (k < 0) || (k > Nfreq)) {
                cout << "Error with index " <<omega[j]-omega[i] << "\t" <<  k << "\t" << hfreq << "\n";
                char ch; cin >> ch;
            }
            Pone[i] += Am[j] * Ap[indx(omega[j]-omega[i])];
            Ptwo[i] += Ap[j] * Am[indx(omega[j]-omega[i])];
        }
        Pone[i] *= pi * hfreq;
        Ptwo[i] *= pi * hfreq;
        //cout << "\t" << omega[i] << "\t" << Pone[i] << "\t" << Ptwo[i] << "\n";
    }
    //char ch; cin >> ch;
  
}

void ipt_t::compute_sigma() {
    for (int i=0; i < Nfreq+1; i++) {
        Sigma[i] = complex<double>(0. , 0.);
        Sigma[i].imag() += 0.5*Ap[indx(omega[i]-omega[0])] * Ptwo[0] + Am[indx(omega[i]-omega[0])]*Pone[0];
        Sigma[i].imag() += 0.5*Ap[indx(omega[i]-omega[Nfreq])] * Ptwo[Nfreq] + Am[indx(omega[i]-omega[Nfreq])]*Pone[Nfreq];
        for (int j=1; j < Nfreq; j++) {
            Sigma[i].imag() += Ap[indx(omega[i]-omega[j])] * Ptwo[j] + Am[indx(omega[i]-omega[j])]*Pone[j];
        }
        Sigma[i].imag() *= -U_on * U_on * hfreq;
    }
    for (int i=0; i < Nfreq+1; i++) {
        for (int j=0; j < Nfreq+1; j++) {
            if ( j != i ) Sigma[i].real() += Sigma[i].imag() / (omega[i] - omega[j]);
        }
        Sigma[i].real() *= -hfreq/pi;
    }
    //cout << "Sigma :\n";
    //for (int i=0; i < Nfreq+1; i++) cout << omega[i] << "\t" <<Sigma[i].real() << "\t" << Sigma[i].imag() << "\n";
    //char ch; cin >> ch;
}


void ipt_t::compute_G() {
    for (int i=0; i < Nfreq+1; i++) {
        G[iter][i] = Hilbert(omega[i] - Sigma[i]);
    }
    //cout << "Hilbert transform -- G :\n";
    //for (int i=0; i < Nfreq+1; i++) cout << omega[i] << "\t" <<G[i].real() << "\t" << G[i].imag() << "\n";
    //char ch; cin >> ch;
}

void ipt_t::dyson() {
    for (int i=0; i < Nfreq+1; i++) {
        G0[iter+1][i] = 1./(1./G[iter][i] + Sigma[i]);
        diff[iter] += abs(G0[iter+1][i] - G0[iter][i]);
    }
    double alpha = 1.0;
    if (iter > 1) {
        for (int i=0; i < Nfreq+1; i++) G0[iter+1][i] = alpha * G0[iter+1][i] + (1-alpha) * G0[iter][i];
    }
    //cout << "Dyson -- g0 :\n";
    //for (int i=0; i < Nfreq+1; i++) cout << omega[i] << "\t" <<G0[i].real() << "\t" << G0[i].imag() << "\n";
    //char ch; cin >> ch;
}

void ipt_t::read_G0() {
  ifstream infile("IPT_G0_in");
    if (infile.fail()) {
        cout << "# Error with reading file IPT_G0_in!\n";
        char ch; cin >> ch;
    }
  for (int i=0; i < Nfreq+1; i++) {
      infile >> G0[0][i].real() >> G0[0][i].imag();
  }
    if (infile.fail()) {
        cout << "# Error with reading file IPT_G0_in!\n";
        char ch; cin >> ch;
    }
  infile.close();
}

void ipt_t::print_G0() {
    ofstream offile("IPT_G0_out");
        for (int i=0; i < Nfreq+1; i++) {
        offile << G0[max_iter][i].real() << "\t" <<  G0[max_iter][i].imag() << "\n";
    }
    offile.close();
}




