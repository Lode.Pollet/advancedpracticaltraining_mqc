#include <vector>
#include <iostream>
#include <cmath>
#include <complex>
#include <fstream>

using namespace std;

const double pi = atan(1.)*4;
const double t_hop = 0.5;
const double U_on = 4.0;
const double beta = 40;
const int Nfreq = 200;
//const double omega_max = 4.;
//double dw = omega_max / Nf;
const int Nt = 2000;
double dt = beta / Nt;
const int Nloop = 100;
const double delta = 0.05;
const double alpha = 0.3;

complex<double> large_tail_omega(const double c1, const double omega) {
    //return (c1/(omega + complex<double>(0.,delta)));
    complex<double> iw(0., omega);
    return( c1/iw );
}

double large_tail_tau(const double c1, const double tau) {
    //return (c1*exp(-delta * tau));
    return (-0.5 * c1);
}

template <typename T>
void pade(const int Npade, const vector<double>& iw_n, const vector<complex<double> >& green_w, const int w_max) {
    // Following :  Vidberg and Serene
    
    // function C_N(z) = a_1 / (1+ a_2 (z-z_1) / ( 1 + ...))
    // C_N(z_i) = u_i, i = 1, ..., N
    // a_i = g(z_i), g_1(z1) = u_i, i = 1, ..., N
    // g_p(z) = (g_{p-1}(z_{p-1}) - g_{p-1}(z) ) / (g_{p-1}(z)(z-z_{p-1}))
    
    if (Npade %2 != 0) {
        cerr << "# Make number of Pade points even!\n";
        char ch; cin >> ch;
    }
    
    if (Npade > Nfreq) {
        cerr << "# Make number of pade extrapolants smaller than number of frequencies!\n";
        char ch; cin >> ch;
    }
    
    
    vector<complex<T> > a_coeff(Npade);
    vector<vector<complex<T> > > g;
    vector<complex<T> > z(Npade);
    vector<complex<T> > u(Npade);
    g.resize(Npade);
    for (int i = 0; i < Npade; i++) g[i].resize(Npade);
    
    // set values
    for (int i =0; i < Npade; i++) z[i] = complex<T>(0. ,iw_n[i]);
    for (int i =0; i < Npade; i++) u[i] = green_w[i];
    
    // initialize
    for (int i =0; i < Npade; i++) g[0][i] = u[i];
    
    // recursion relation
    for (int p =1; p < Npade; p++) {
        for (int i =p; i < Npade; i++) {
            g[p][i] = (g[p-1][p-1]/g[p-1][i] - complex<T>(1.,0)) / (z[i] - z[p-1]) ;
        }
    }
    
    
    
    // take coefficients
    for (int i = 0; i < Npade; i++) a_coeff[i] = g[i][i];
    
    // let's check...
    /*
     for (int i = 0; i < Npade; i++) {
     complex<T> C = a_coeff[Npade-1] * (z[i] - z[Npade-2]) + 1.;
     for (int p = Npade-2; p > 0; p--) C = 1./C * (z[i] - z[p-1]) * a_coeff[p] + 1.;
     C = 1./C * a_coeff[0];
     cout << z[i].imag() << "\t" << g[0][i].real() << "\t" << g[0][i].imag() << "\t" << C.real() << "\t" << C.imag() << "\n";
     }
     char ch; cin >> ch;
     //exit(1);
     // OK
     */
    
    // output
    stringstream str_outpade;
    str_outpade << "qc_pade_" << Npade;
    ofstream outpade(str_outpade.str().c_str());
    double s_pade = 0.;
    double h_pade = 0.02;
    //int w_max = static_cast<int> (max_freq/h_pade);
    int Np = static_cast<int>(w_max*2/h_pade);
    for (int i = 0; i < Np+1; i++) {
        //complex<double> omega = complex<double>(0., i*0.01);
        T omega = -w_max + i * h_pade;
        complex<T> C = a_coeff[Npade-1] * (omega - z[Npade-2]) + complex<T>(1.,0);
        for (int p = Npade-2; p > 0; p--) C = complex<T>(1.,0)/C * (omega - z[p-1]) * a_coeff[p] + complex<T>(1.,0);
        C = complex<T>(1.,0)/C * a_coeff[0];
        //cout << omega.imag() << "\t" << C.real() << "\t" << C.imag() << "\n";
        outpade << omega << "\t" << C.real() << "\t" << C.imag() << "\n";
        s_pade += h_pade * C.imag();
    }
    outpade << endl << endl << "# integral : " << s_pade << "\n";
    outpade.close();
    
    return;
}



void Fourier(const vector<double>& G_in, vector<complex<double> >& G_out, const double c1) {
    for (int iw = 0; iw < Nfreq; iw++) {
        double omega = 2*(iw+0.5)*pi/beta;
        G_out[iw] = 0;
        for (int it = 1; it < Nt+1; it++) {
          double tau = it * dt;
          G_out[iw] +=  (G_in[it]-large_tail_tau(c1, tau)) * complex<double>(cos(omega*tau) , sin(omega*tau) ) ;
        }
        G_out[iw] += 0.5 * (G_in[0]-large_tail_tau(c1, 0.) + G_in[Nt]-large_tail_tau(c1, beta)  );
        G_out[iw] *= dt;
        G_out[iw] += large_tail_omega(c1, omega);
    }
}

void InverseFourier(const vector<complex<double> >& G_in, vector<double>& G_out, const double c1) {
    for (int it = 0; it < Nt+1; it++) {
        G_out[it] = 0.;
        double tau = it * dt;
        for (int iw = 0; iw < Nfreq; iw++) {
            //double omega = -omega_max + iw * dw;
            double omega = 2*(iw+0.5) * pi / beta;
            G_out[it] += (real(G_in[iw] - large_tail_omega(c1,omega))*cos(omega*tau) + imag(G_in[iw]- large_tail_omega(c1,omega))*sin(omega*tau));
        }
        //G_out[it] *= dw / (2*pi);
        G_out[it] *= 2/beta;
        G_out[it] += large_tail_tau(c1, tau);
    }
}


int main() {
 
  vector<double> omega(Nfreq);
  for (int iw=0; iw < omega.size(); iw++) omega[iw] = 2*(iw+0.5) * pi / beta;

  vector<double> G0_t(Nt+1);
  vector<double> G_t(Nt+1);
  vector<double> Sigma_t(Nt+1);
  
  vector<complex<double> > G0_w(Nfreq);
  vector<complex<double> > G0_new(Nfreq);
  vector<complex<double> > G_w(Nfreq);
  vector<complex<double> > Sigma_w(Nfreq);
  vector<double> diff(Nloop,0.);
    
  /*
    for (int i=0; i < Nt+1; i++) {
        double t = i*dt;
        G0_t[i] = -0.5*cos(t*(beta-t)/beta/2/pi);
    }
    Fourier(G0_t, G0_w, 1.);
    //InverseFourier(G0_w, G_t, 1.);
   
    
  //for (int i=0; i < Nt; i++) cout << i*dt << "\t" << G0_t[i] << "\t" << G_t[i] << "\n";   // test if agree and norm OK
  //for (int i=0; i < Nfreq; i++) cout << omega[i] << "\t" << real(G0_w[i]) << "\t" << imag(G0_w[i]) << "\n"; // test if imag going as 1/w for large w
  */

  
  for (int i=0; i < omega.size(); i++) {
    //double s = (abs(omega[i]) < 1.) ? sqrt(1 - omega[i]*omega[i])*2./pi : 0.;
    //G_w[i] = complex<double>(omega[i]/(omega[i]*omega[i] + s*s), s/(omega[i]*omega[i] + s*s));
    G_w[i] = 1./complex<double>(0, omega[i]);
    //  G_w[i] = G0_w[i];
  }
  //for (int i=0; i < Nfreq; i++) cout << omega[i] << "\t" << real(G_w[i]) << "\t" << imag(G_w[i]) << "\n";
    
    //InverseFourier(G_w, G_t, 1.);
    //for (int i=0; i < omega.size(); i++) G_w[i] /= G_t[0];
    //for (int i=0; i < Nt+1; i++) cout << i*dt  << "\t" << G_t[i] << "\n";
  
  
    
  for (int n=0; n < Nloop; n++) {
    for (int i=0; i < omega.size(); i++) G0_new[i] = 1./(complex<double>(0,omega[i]) - t_hop * t_hop * G_w[i]  );
    if (n==0) {
      for (int i=0; i < omega.size(); i++) G0_w[i] = G0_new[i];
    }
    else {
      for (int i=0; i < omega.size(); i++) diff[n] += abs(G0_new[i] - G0_w[i]);
      for (int i=0; i < omega.size(); i++) G0_w[i] = alpha * G0_new[i] + (1-alpha) * G0_w[i];
    }
    InverseFourier(G0_w, G0_t, 1.);
    for (int i=0; i < Sigma_t.size(); i++) Sigma_t[i] = U_on * U_on * G0_t[i] * G0_t[i] * G0_t[i];
    Fourier(Sigma_t, Sigma_w, 0.);
    for (int i=0; i < omega.size(); i++) G_w[i] = 1./(1./G0_w[i] - Sigma_w[i]);
    InverseFourier(G_w, G_t, 1.);
    cout << "# Iteration nr :" << n << "\t" << diff[n] << "\n";
    if ((n > 5) && (diff[n] < 0.0001)) break;
    //char ch; cin >> ch;
      
  }
  
  for (int i=0; i < Nt+1; i++) cout << i*dt  << "\t" << G0_t[i] << "\t" << G_t[i] << "\t" << Sigma_t[i] << "\n";
  //for (int i=0; i < omega.size(); i++) cout << omega[i] << "\t" <<  real(G_w[i]) << "\t" << imag(G_w[i])  << "\n";
  
    pade<double>(100, omega, G_w, 4.0);
    
}

