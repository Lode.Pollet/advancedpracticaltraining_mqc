#include "ipt.hpp"
#include <exception>
#include <sstream>

int main(int argc, char** argv) {
  IPT_t myIPT;
  try {
    myIPT.read_params();
    myIPT.init();
    myIPT.print_params(cout);
  } catch (const char* e) {
    cerr << "\n# " << e << "\n";
	return(1);
  }
  
  for (int iter = 0; iter < myIPT.get_max_iter(); iter++) {
    cout << "\n\n\n# Iteration nr : " << iter ;
	myIPT.iteration(iter);
	ostringstream outs;
	
       
        outs << "iter_omega_" << iter;
	//cout << "name of file : " << outs.str() << endl;
	ofstream outFile(outs.str().c_str());
	myIPT.print_vec_omega(outFile);
	outFile.close();

	ostringstream outtau;
        outtau << "iter_tau_" << iter;
	//cout << "name of file : " << outtau.str() << endl;
	ofstream outFile2(outtau.str().c_str());
	myIPT.print_vec_tau(outFile2);
	outFile2.close();

	double is_conv = myIPT.calc_selfcon();
	cout << "# Iteration nr : " << iter << " mu : " << myIPT.get_mu() << "\tdensity : "<< myIPT.get_density() << "\tselfcon error : "<< is_conv << "\n";
	if (is_conv < myIPT.get_tolerance()) break;
  }
  //myIPT.pade(myIPT.get_Nfreq() );
    myIPT.pade(150);
  return(0);
}
