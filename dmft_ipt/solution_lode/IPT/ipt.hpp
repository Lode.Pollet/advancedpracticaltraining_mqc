#ifndef IPT_HPP
#define IPT_HPP

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <complex>
#include <iomanip>
#include <exception>
#include "fftw3.h"

using namespace std;

class IPT_t {
public:
  IPT_t() : pi(3.14159265358979323846) {}
  void read_params();
  void print_params(ostream&);
  void init();
  void calc_mu_tilde();
  void calc_green0();
  void calc_n0();
  void calc_A();
  void calc_B();
  void calc_sigma();
	void calc_sigma_interpolate();
  void calc_green_full();
  void calc_f1();
  void calc_f2();
  void calc_density();
  void iteration(const int);
	void init_green();
  int get_Nfreq() { return Nfreq;};
  double get_f1() { return (f1);}
  double get_f2() { return (f2);}
  double get_mu() { return (mu);}
  double get_density() { return (density);}
  double calc_selfcon();
  int get_max_iter() { return (max_iter);}
  double get_tolerance() {return (tolerance);}
  void print_vec_omega(ostream&);
	void print_vec_tau(ostream&);
	
	// void do_FFT_forward(const vector<complex<double> >&, vector<complex<double> >&, const int, const double);
	// void do_FFT_backward(const vector<complex<double> >&, vector<complex<double> >&, const int, const double);
	
	
	double DOS_cubic_lattice(const double E) const {
		if (E < 0) return (DOS_cubic_lattice(-E));
		if (E > 3) {
			return (0);
		}
		else if (E < 1) {
			return(70.7801 + 1.0053*E*E);
		}
		else {
			double t1 = 80.3702 - 16.3846*(3-E) + 0.78978*(3-E)*(3-E);
			double t2 = -44.2639 + 3.66394*(3-E) - 0.17248*(3-E)*(3-E);
			return( sqrt(3-E)*(t1 + t2*sqrt(E-1)));
		}
	}
	double eps_sq;
	vector<double> DOS_eps;
	vector<double> DOS;
	int zcoord;
	
	void forward_fourier(const vector<double>& G_in, vector<complex<double> >& G_out, const double c1);
	void backward_fourier(const vector<complex<double> >& G_in, vector<double>& G_out, const double c1);
	
	double large_tail_tau(const double c1, const double tau) const {
		//double t1 = (tau/beta - 0.5);
		//return (c1*t1);
		return (-0.5 * c1);
	}
	
	complex<double> large_tail_omega(const double c1,  const double omega) const {
		complex<double> iw(0., omega);
		return( c1/iw );
	}
   void pade(const int);
	

	
private:
  const double pi;
  int Nfreq;
  int Ntimes;
  double max_freq;
  double eta;
	double beta;
	double dt;
  double t_hop;
  double U_on;
  double mu;
  double mu_tilde;
  double target_density;
  double density;
  double coeff_A;
  double coeff_B;
  double n0;
  double f1;
  double f2;
  double tolerance;
  int max_iter;
  vector<double> green_t;
	vector<complex<double> > green_w;
  vector<complex<double> > green0_w;
	vector<complex<double> > Glatt_w;
	vector<double> green0_t;
	vector<double> Glatt_t;
  vector<complex<double> > sigma_w;
	vector<complex<double> > sigma_interpolate_w;
	vector<double> sigma_t;
	vector<double> sigma_old_t;
  //vector<double> omega;
	vector<complex<double> > iw_n;
};

#endif
