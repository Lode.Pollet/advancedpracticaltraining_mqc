#include "ipt.hpp"

void IPT_t::read_params() {
  ifstream inFile("params");
  if (inFile.fail()) throw "Unable to open params";
  string s;
  inFile >> Ntimes; getline(inFile, s);
  inFile >> Nfreq; getline(inFile, s);
  inFile >> max_freq; getline(inFile, s);
  inFile >> eta; getline(inFile, s);
	inFile >> beta; getline(inFile, s);
  inFile >> t_hop; getline(inFile, s);
  inFile >> U_on; getline(inFile, s);
  inFile >> mu; getline(inFile, s);
  inFile >> target_density; getline(inFile, s);
  inFile >> tolerance; getline(inFile, s);
  inFile >> max_iter; getline(inFile, s);
  if (inFile.fail()) throw "Unable to read params";
  inFile.close();
}

void IPT_t::init() {
	cout << setprecision(12);
	//Ntimes = Nfreq * 100;
	dt = beta / Ntimes;
  density = target_density;
	Glatt_w.resize(Nfreq);
  green_t.resize(Ntimes+1);
	green_w.resize(Nfreq);
  green0_t.resize(Ntimes+1);
	Glatt_t.resize(Ntimes+1);
	green0_w.resize(Nfreq);
  sigma_t.resize(Ntimes+1);
	sigma_old_t.resize(Ntimes+1);
	sigma_w.resize(Nfreq);
  sigma_interpolate_w.resize(Nfreq);
	//omega.resize(Nfreq);
	iw_n.resize(Nfreq);
	//max_freq = Nfreq / 2.;
 	mu_tilde = mu - U_on *target_density;
  for (int i = 0; i < Nfreq; i++) {
		iw_n[i] = complex<double>( 0., pi * (2* i+1) / beta ); // let's keep only positive frequencies
    //iw_n[i] = complex<double>( 0., pi * (2* (i - max_freq) +1) / beta ); 
	  green_w[i] = (0. , 0.);
		green0_w[i] = 1./(iw_n[i] + mu_tilde);
		sigma_w[i] = (0., 0.);
		sigma_interpolate_w[i] = complex<double>(U_on * target_density,0.);
		Glatt_w[i] = 0;
	}
	for (int itimes = 0; itimes < Ntimes +1; itimes++) {
		green0_t[itimes] = 0;
		green_t[itimes] = 0;
		sigma_t[itimes] = 0.;
		sigma_old_t[itimes] = 0.;
	}
	backward_fourier(green0_w, green0_t, 1.);
	//init_green();
	cout << "# Initial mu_tilde : " << mu_tilde << "\n";
	//char ch; cin >> ch;
	
	if (Ntimes <= Nfreq*2) {
		cerr << "# Increase Ntimes : " << Ntimes << "\tNfreq : " << Nfreq << "\n";
	        char ch; cin >> ch;	
	}
	
	
	
	double norm = 0.;
	zcoord = 6;
	int Np = 120001;
	double h = (zcoord*2.*t_hop) / (Np-1);
	for (int i = 0; i < Np; i++) {
		double E = -zcoord*t_hop + i*h;
		DOS.push_back(E);
		DOS_eps.push_back(DOS_cubic_lattice(E/2.));
	}
	for (int i = 1; i < Np; i+=2) {
		norm += DOS_eps[i] * 4.;
	}
	for (int i = 2; i < Np-1; i+=2) {
		norm += DOS_eps[i] * 2.;
	}
	norm *= h/3;
	double eps_sq =0.;
	for (int i =0; i < Np; i++) {
		DOS_eps[i] /= norm;
	}
	norm = 0.;
	for (int i = 1; i < Np; i+=2) {
		eps_sq += DOS[i] * DOS[i] * DOS_eps[i] * 4.;
		norm += DOS_eps[i] * 4.;
	}
	for (int i = 2; i < Np; i+=2) {
		eps_sq += DOS[i] * DOS[i] * DOS_eps[i] * 2.;
		norm += DOS_eps[i] * 2.;
	}
	norm *= h/3;
	eps_sq *= h/3.;
	//for (int i = 0; i < Np; i++) cout << DOS[i] << "\t" << DOS_eps[i] << "\n";
	cout << "# DOS eps_sq : " << eps_sq << "\tnorm : " << norm << "\tsize : " << DOS.size() << "\n";
}


void IPT_t::print_params(ostream& os) {
  os << "# Iterative Perturbation Theory scheme using H. Kajueter and G. Kotliar, Phys Rev Lett 77, 131 (1996).\n";
	os << "# number of time points : " << Ntimes << endl;
  os << "# number of frequencies : " << Nfreq << endl;
  os << "# maximum frequency     : " << max_freq << endl; 
  os << "# eta                   : " << eta << endl; 
	os << "# beta                  : " << beta << endl;
  os << "# t_hop                 : " << t_hop << endl;
  os << "# U_on                  : " << U_on << endl; 
  os << "# mu (initial)          : " << mu << "\t mu - Un :" << mu - U_on *target_density << endl; 
  os << "# target_density        : " << target_density << endl; 
  os << "# Tolerance             : " << tolerance << endl;
  os << "# Max nr itarations     : " << max_iter << endl;
  os << "\n\n\n\n";
}

void IPT_t::iteration(const int nr) {
	calc_green0();
	cout << "\n";
  cout << "\t\t ### green0(w=0) : " <<  green0_w[Nfreq/2] << "\n";
  calc_n0();
  cout << "\t\t ### n0 : " << n0 << "\t" << -green0_t[Ntimes] << "\n";
	calc_A();
  cout << "\t\t ### coeff A : " << coeff_A << "\n";
  calc_B();
  cout << "\t\t ### coeff B : " << coeff_B << "\n";
  calc_sigma();
	calc_sigma_interpolate();
  cout << "\t\t ### sigma(w=0) : " <<  sigma_w[Nfreq/2] << "\t" << sigma_interpolate_w[Nfreq/2] << "\n";
  //calc_green_full();
  //cout << "\t\t ### green_full(w=0) : " <<  green_w[Nfreq/2] << "\n";
	//calc_density();
	//cout << "\t\t ### density : " << density  /* << "\t" << -green_t[Ntimes] << */  "\n";
	calc_mu_tilde();
  cout << "\t\t ### mu_tilde : " << mu_tilde << "\n";
	cout << "\t\t ### density : " << density << "\n";
	calc_f1();
  cout << "\t\t ### f1 : " << f1 << "\n";
  //calc_f2();
  cout << endl;
  //print_vec(cout);
	//char ch; cin >> ch;
}

void IPT_t::calc_mu_tilde() {
	// some kind of bisection such that density is preserved...
	//mu_tilde = mu - U_on * density;
	// half filling : mu_tilde = 0;
	if (target_density == 0.5) {
		mu_tilde = mu - U_on*density;
		calc_green_full();
		calc_density();
		backward_fourier(green_w, green_t, 1.);
		return;
	}
	
	/*
	for (int i = 0; i < 100; i++) {
		mu_tilde = 10*mu - U_on * (target_density * 0.2*i);
		calc_green_full();
		calc_density();
		cout << mu_tilde << "\t" << density << "\n";
	}
        char ch; cin >> ch;	
	*/
	
	
	double mu_low, mu_up, mu_mid;
	double n_up, n_low, n_mid;
	mu_low = mu_tilde + U_on; 
	mu_up  = mu_tilde - U_on; 
	
	for (;;) {
	  mu_tilde = mu_low;
	  calc_green_full();
	  calc_density();
	  n_low = density;
	  //if (n_low > target_density) {
		//  cerr << "\n# Error in bisection : mu_low is too low : " << mu_low << "\t" << n_low << "\n";
		//  exit(1);
	  //}
		if (n_low < target_density) break;
		cout << "\t\t# Determining mu_low : " << mu_low << "\t" << n_low << "\n";
		mu_low += U_on;
	}
	
	for (;;) {
	  mu_tilde = mu_up;
	  calc_green_full();
	  calc_density();
	  n_up = density;
	  //if (n_up < target_density) {
		//  cerr << "\n# Error in bisection : mu_up is too high : " << mu_up << "\t" << n_up << "\n";
		//  exit(1);
	  //}
		if (n_up > target_density) break;
		cout << "\t\t# Determining mu_up : " << mu_up << "\t" << n_up << "\n";
		mu_up -= U_on;
	}
	
	for (int ibisec = 0; ibisec < 100; ibisec++) {
		mu_mid = 0.5 * (mu_up + mu_low);
		mu_tilde = mu_mid;
		calc_green_full();
		calc_density();
		n_mid = density;
		if (abs(n_mid - target_density) < tolerance) break;
				cout << "\t\t# Bisection step " << ibisec << "\t mu_low mu_mid mu_up : " << mu_low << "\t" << mu_mid << "\t" << mu_up << "\n";
		cout << "\t\t# Bisection step " << ibisec << "\t n_low  n_mid  n_up  : " << n_low << "\t" << n_mid << "\t" << n_up << "\n\n";
		if (n_mid < target_density) {
			mu_low = mu_mid;
			n_low = n_mid;
		}
		else {
			mu_up = mu_mid;
			n_up = n_mid;
		}
	}
	
	cout << "# Finished bisection and found solution : " << mu_tilde << "\t" << density << "\n";
	backward_fourier(green_w, green_t, 1.);
}

void IPT_t::calc_green0() {
	// Hilbert transform
	
	
	// Cubic lattice
	double DOS_norm = 0.;
	int DOS_Np = DOS.size();
	
	for (int iw = 0; iw < Nfreq; iw++) {
	  for (int ieps = 1; ieps < DOS_Np; ieps += 2) {
		  double eps = DOS[ieps];
		  Glatt_w[iw]    += 4 * DOS_eps[ieps] / ( iw_n[iw] + mu - mu_tilde -eps - sigma_interpolate_w[iw] );
	  }
	  for (int ieps = 2; ieps < DOS_Np-1; ieps += 2) {
		  double eps = DOS[ieps];
		  Glatt_w[iw]    += 2 * DOS_eps[ieps] / ( iw_n[iw] + mu - mu_tilde -eps - sigma_interpolate_w[iw] );
	  }
	  Glatt_w[iw]    *= (DOS[1] - DOS[0])/3;
	}
	
	for (int iw = 0; iw < Nfreq; iw++) {
		complex<double> invg0 = (1./Glatt_w[iw] - mu + sigma_interpolate_w[iw]) + complex<double>(mu_tilde, 0.);
		green0_w[iw] = 1./invg0;
	}
	
	//for (int i = 0; i < Nfreq; i++) cout << imag (iw_n[i]) << "\t" << real(Glatt_w[i]) << "\t" << imag(Glatt_w[i]) << "\t" << real(green0_w[i]) << "\t" << imag(green0_w[i]) << "\n";
	//exit(1);
	
	
	
	// Bethe lattice
	 // for (int i = 0; i < Nfreq; i++) green0_w[i] = 1. / ( iw_n[i] + mu_tilde + t_hop * t_hop * green_w[i]);
	
	
	// 
	//do_FFT_backward(green0_w, green0_t, Nfreq, dt);
	 
	
	backward_fourier(green0_w, green0_t, 1.);
	
	//vector<complex<double> > G0_w(Nfreq);
	//forward_fourier(green0_t, G0_w, 1.);
	 //printing and testing
	 //for (int i = 0; i < Ntimes+1; i++) cout << i*dt << "\t" << green0_t[i] << "\n"; // OK
	 //for (int i = 0; i < Nfreq; i++) cout << imag (iw_n[i]) << "\t" << real(green0_w[i]) << "\t" << imag(green0_w[i]) << "\t" << real(green_w[i]) << "\t" << imag(green_w[i]) << "\n";
	// exit(1);
}

void IPT_t::calc_n0() {
	double s  = 0;
	for (int i = 0; i < Nfreq; i++) s += real(green0_w[i])/beta;
	n0 = 2*s + 0.5;
}

void IPT_t::calc_A() {
  coeff_A = (n0 > 0 ? density*(1 - density) / (n0*(1-n0)) : 0);
	if (target_density == 0.5) coeff_A = 1.;
}


void IPT_t::calc_B() {
  coeff_B = (n0 > 0 ? ( (1-density)*U_on - mu + mu_tilde ) / (n0*(1-n0)*U_on*U_on) : 0);
	if (target_density == 0.5) coeff_B = 0.;
}

void IPT_t::calc_sigma() {
	
	// calculate self energy
	for (int i = 0; i < Ntimes+1;i++) {
		sigma_t[i] = green0_t[i]*green0_t[i]*green0_t[Ntimes-i]*U_on*U_on;
	}
  		
	// printing
	//for (int i =0; i < Ntimes+1; i++) {
	//	cout << i*dt << "\t" << sigma_t[i] << "\t" << green0_t[i] << "\n";
	//}
	//exit(1);
	
	// fourier back
	//do_FFT_forward(Self_t, sigma, Nfreq, dt);
	forward_fourier(sigma_t, sigma_w, 0.);
	//forward_fourier(sigma_t, sigma_w, n0*(1-n0) * U_on * U_on);
	
	
	// printing
	// for (int i =0; i < Nfreq; i++) {
	//	cout << imag(iw_n[i]) << "\t" << real(sigma_w[i]) << "\t" << imag(sigma_w[i]) << "\t" << 0.25*U_on*U_on*real(green0_w[i]) << "\t" << 0.25*U_on*U_on*imag(green0_w[i]) << "\n";
	//}
	//exit(1);
	
	// atomic
	//for (int i =0; i < Nfreq; i++) sigma_w[i] = n0 * (1-n0) * green0_w[i];
	
	
	
}

void IPT_t::calc_sigma_interpolate() {
	for (int i = 0; i < Nfreq; i++) sigma_interpolate_w[i] = U_on * density + coeff_A*sigma_w[i] / ( 1. - coeff_B * sigma_w[i]);
}

void IPT_t::calc_green_full() {
  if (target_density == 0.5) {
		for (int i =0; i < Nfreq; i++) {
			green_w[i] = 1./ (1./green0_w[i] + mu - sigma_w[i] - U_on*target_density);
			//cout << imag(iw_n[i]) << "\t" << real(green_w[i]) << "\t" << imag(green_w[i]) << "\n";
		}
	}
	else {
	  for (int i = 0; i < Nfreq; i++) {
      green_w[i] = 1. / (1./green0_w[i]  - mu_tilde + mu - sigma_interpolate_w[i] );
		  //cout << imag(iw_n[i]) << "\t" << real(green_w[i]) << "\t" << imag(green_w[i]) << "\n";
	  }
	}
    ofstream offile("green_w");
    for (int i=0; i < Nfreq; i++) {
        offile << imag(iw_n[i]) << "\t" << green_w[i].real() << "\t" << green_w[i].imag() << "\n";
    }
    offile.close();
	//exit(1);
	
	// testing and printing
	//backward_fourier(green_w, green_t, 1.);
		
	//for (int i =0; i < Ntimes+1; i++) {
	//	cout << i*dt << "\t" << green_t[i] << "\n";
	//}
  //exit(1);
	
}

void IPT_t::print_vec_omega(ostream& os) {
	os << "# n0 : " << n0 << endl;
	os << "# n  : " << density << endl;
	os << "# A, B :  " << coeff_A << "\t" << coeff_B << endl << "\n";
	os << "# i omega	real(hybrid) imag(hybrid)	real(G0)	imag(G0)	real(sigma)	imag(sigma)	real(sigma_interpol)	imag(sigma_interpol) real(G)	imag(G) " << endl;
  for (int i = 0; i < Nfreq; i++) {
	os << i << " " << imag(iw_n[i]) << " "   
	   << green0_w[i].real() << " " << green0_w[i].imag() << " "
	   << sigma_w[i].real() << " " << sigma_w[i].imag() << " " 
		 << sigma_interpolate_w[i].real() << " " << sigma_w[i].imag() << " " 
	   << green_w[i].real() << " " << green_w[i].imag() << " "
	   << "\n";
  }
}

void IPT_t::print_vec_tau(ostream& os) {
	os << "# tau \t G0(tau) \t G(tau) \n";
	for (int i =0; i < Ntimes+1; i++) {
		os << i*dt << "\t" << green0_t[i]  << "\t"  << sigma_t[i] << "\t" << green_t[i] <<  "\n";
	}
}

void IPT_t::calc_density() {
  // double s = 0;
  // int i = -1;
  // for (;;) {
  //   i++;
  //  if (omega[i] >= 0) break;
	// s += h_freq * green_w[i].imag();
  //}
  //density = s;
	double s  = 0;
	for (int i = 0; i < Nfreq; i++) s += real(green_w[i])/beta;
	density = 2*s + 0.5;

}

void IPT_t::calc_f1() {
  f1 = target_density - density;
}


void IPT_t::calc_f2() {
}


double IPT_t::calc_selfcon() {
  double s = 0;
  for (int i = 0; i < Ntimes+1; i++) {
    s += abs(sigma_old_t[i] - sigma_t[i]);
		sigma_old_t[i] = sigma_t[i];
  }
  return (s/Ntimes);
}


/*
void IPT_t::do_FFT_forward(const vector<complex<double> >& G_in, vector<complex<double> >& G_out, const int N, const double dt) {
	fftw_complex *in, *out;
	fftw_plan p;
	in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
	out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
	
	for (int i = 0; i < N; i++) {
		//in[i] = G_in[i];
		complex<double> phase(cos(pi*i*((Nfreq+1.)/Nfreq)), sin(pi*i*(Nfreq+1.)/Nfreq));
		in[i][0] = -real(G_in[i] * conj(phase))*dt;
		in[i][1] = -imag(G_in[i] * conj(phase))*dt;
	}
	
	p = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
	
	fftw_execute(p);
	
	for (int i = 0; i < N; i++) {
		//G_out[i] = out[i] / sqrt(N *1.);
		//G_out[i] = complex<double>(out[i][0], out[i][1]) / sqrt(N*1.);
		G_out[i] = complex<double>(out[i][0], out[i][1]);
	}
	
	fftw_destroy_plan(p);
	fftw_free(in); fftw_free(out);
}

void IPT_t::do_FFT_backward(const vector<complex<double> >& G_in, vector<complex<double> >& G_out, const int N, const double dt) {
	fftw_complex *in, *out;
	fftw_plan p;
	in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
	out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
	
	for (int i = 0; i < N; i++) {
		//in[i] = G_in[i];
		in[i][0] = -real(G_in[i]);
		in[i][1] = -imag(G_in[i]);
	}
	
	p = fftw_plan_dft_1d(N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
	
	fftw_execute(p); 
	
	for (int i = 0; i < N; i++) {
		//G_out[i] = out[i] / sqrt(N *1.);
		//G_out[i] = complex<double>(out[i][0], out[i][1]) / sqrt(N*1.);
		complex<double> phase(cos(pi*i*((Nfreq+1.)/Nfreq)), sin(pi*i*(Nfreq+1.)/Nfreq));
		G_out[i] = complex<double>(out[i][0], out[i][1]) / (N*dt) * phase;
	}
	
	fftw_destroy_plan(p);
	fftw_free(in); fftw_free(out);
}

*/

void IPT_t::forward_fourier(const vector<double>& G_in, vector<complex<double> >& G_out, const double c1) {
	/*
	// zero frequency
	G_out[0] = complex<double>(0.,0.);
	for (int it = 1; it < Ntimes; it=it+2) {
		double tau = it * dtime;
		G_out[0] +=  4 * G_in[it];
	}
	for (int it=2; it< Ntimes; it=it+2) {
		double tau = it*dtime;
		G_out[0] +=  2 * G_in[it];
	}
	G_out[0] += G_in[0] + G_in[Ntimes];
	G_out[0] *= dtime/3;
	
	
	// other frequencies
	for (int iw = 1; iw < Nfreq; iw++) {
		G_out[iw] = complex<double>(0.,0.);
		double omega = iw * domega;
		for (int it = 1; it < Ntimes; it=it+2) {
			double tau = it * dtime;
			G_out[iw] +=  4 * (G_in[it]-large_tail_tau(c1, c2, c3, tau)) * complex<double>(cos(omega*tau) , sin(omega*tau) ) ;
		}
		for (int it=2; it< Ntimes; it=it+2) {
			double tau = it*dtime;
			G_out[iw] +=  2 * ( G_in[it] - large_tail_tau(c1, c2, c3, tau)) * complex<double>(cos(omega*tau) , sin(omega*tau));
		}
		//G_out[iw] += G_in[0] + G_in[Ntimes] * complex<double>(cos(omega*Nfreq*dtime) , sin(omega*Nfreq*dtime) );
		G_out[iw] += G_in[0]-large_tail_tau(c1, c2, c3, 0.) + (G_in[Ntimes]-large_tail_tau(c1, c2, c3, beta) );
		G_out[iw] *= dtime/3;
		G_out[iw] += large_tail_omega(c1, c2, c3, omega);
	}
	 */
	
	for (int iw = 0; iw < Nfreq; iw++) {
		G_out[iw] = 0;
		double omega = 2*(iw+0.5)*pi/beta;
		// simpson integration
		for (int it = 1; it < Ntimes; it=it+2) {
			double tau = it * dt;
			G_out[iw] +=  4 * (G_in[it]-large_tail_tau(c1, tau)) * complex<double>(cos(omega*tau) , sin(omega*tau) ) ;
		}
		for (int it=2; it< Ntimes; it=it+2) {
			double tau = it*dt;
			G_out[iw] +=  2 * ( G_in[it] - large_tail_tau(c1,tau)) * complex<double>(cos(omega*tau) , sin(omega*tau));
		}
		G_out[iw] += G_in[0]-large_tail_tau(c1, 0.) + (G_in[Ntimes]-large_tail_tau(c1, beta) );
		G_out[iw] *= dt/3;
		G_out[iw] += large_tail_omega(c1, omega);
		
	}
}

void IPT_t::backward_fourier(const vector<complex<double> >& G_in, vector<double>& G_out, const double c1) {
	/*
	for (int it = 0; it < Ntimes+1; it++) {
		G_out[it] = real(G_in[0])/beta;
		//G_out[it] = real(G_in[0])/beta;
		double tau = it * dtime;
		for (int iw = 1; iw < Nfreq; iw++) {
			//for (int iw = 1; iw < Ntimes/2; iw++) {
			double omega = iw*domega;
			G_out[it] += (real(G_in[iw] - large_tail_omega(c1,c2,c3,omega))*cos(omega*tau) + imag(G_in[iw]- large_tail_omega(c1,c2,c3,omega))*sin(omega*tau))*2./beta;
		}
		G_out[it] += large_tail_tau(c1, c2, c3, tau);
	}
	*/
	for (int it = 0; it < Ntimes+1; it++) {
		G_out[it] = 0.;
		double tau = it * dt;
		for (int iw = 0; iw < Nfreq; iw++) {
			double omega = 2*(iw+0.5) * pi / beta;
			G_out[it] += (real(G_in[iw] - large_tail_omega(c1,omega))*cos(omega*tau) + imag(G_in[iw]- large_tail_omega(c1,omega))*sin(omega*tau))*2./beta;
		}
		G_out[it] += large_tail_tau(c1, tau);
	}
}




void IPT_t::pade(const int Npade) {
	// Following :  Vidberg and Serene
	
	// function C_N(z) = a_1 / (1+ a_2 (z-z_1) / ( 1 + ...))
	// C_N(z_i) = u_i, i = 1, ..., N
	// a_i = g(z_i), g_1(z1) = u_i, i = 1, ..., N
	// g_p(z) = (g_{p-1}(z_{p-1}) - g_{p-1}(z) ) / (g_{p-1}(z)(z-z_{p-1}))
	
	/*
	ifstream inpade("green_w");
	for (int i = 0; i < Nfreq; i++) {
		double v1, v2, v3;
		inpade >> v1 >> v2 >> v3;
		green_w[i] = complex<double>(v2, v3);
		green_w[i] = 1./complex<double>(-2, v1);
		//cout << v1 << "\t" << green_w[i] << "\n";
	}
	inpade.close();
	*/
	 
	//int Npade = 2*Nfreq;
	
	if (Npade %2 != 0) {
		cerr << "# Make number of Pade points even!\n";
	        char ch; cin >> ch;	
	}
	
	if (Npade > Nfreq) {
		cerr << "# Make number of pade extrapolants smaller than number of frequencies!\n";
		char ch; cin >> ch;
	}
	
	
	vector<complex<double> > a_coeff(Npade);
	vector<vector<complex<double> > > g;
	vector<complex<double> > z(Npade);
	vector<complex<double> > u(Npade);
	g.resize(Npade);
	for (int i = 0; i < Npade; i++) g[i].resize(Npade);
	
	// set values
	//for (int i =0; i < Npade/2; i++) u[Npade/2 - i - 1] = conj(green_w[i]);
	//for (int i =0; i < Npade/2; i++) u[i+Npade/2] = green_w[i];
	//for (int i =0; i < Npade/2; i++) z[Npade/2 -i - 1] = -iw_n[i];
	//for (int i =0; i < Npade/2; i++) z[i+Npade/2] = iw_n[i];
	for (int i =0; i < Npade; i++) z[i] = iw_n[i];
	for (int i =0; i < Npade; i++) u[i] = green_w[i];
	
	// initialize
	for (int i =0; i < Npade; i++) g[0][i] = u[i];
	
	// recursion relation
	for (int p =1; p < Npade; p++) {
		for (int i =p; i < Npade; i++) {
			g[p][i] = (g[p-1][p-1]/g[p-1][i] - 1.) / (z[i] - z[p-1]) ;
		}
	}
	
	
	
	// take coefficients
	for (int i = 0; i < Npade; i++) a_coeff[i] = g[i][i];
	
  // let's check...
	/*
	for (int i = 0; i < Npade; i++) {
		complex<double> C = a_coeff[Npade-1] * (z[i] - z[Npade-2]) + 1.;
		for (int p = Npade-2; p > 0; p--) C = 1./C * (z[i] - z[p-1]) * a_coeff[p] + 1.;
		C = 1./C * a_coeff[0];
		cout << z[i].imag() << "\t" << g[0][i].real() << "\t" << g[0][i].imag() << "\t" << C.real() << "\t" << C.imag() << "\n";
	}
    char ch; cin >> ch;
	//exit(1);
	// OK 
	*/
	
		
	
	// output
	stringstream str_outpade;
	str_outpade << "qc_pade_" << Npade;
	ofstream outpade(str_outpade.str().c_str());
	double s_pade = 0.;
	double h_pade = 0.01;
	int w_max = static_cast<int> (max_freq/h_pade);
	for (int i = -w_max; i <= w_max; i++) {
		//complex<double> omega = complex<double>(0., i*0.01);
		double omega = i * h_pade;
		complex<double> C = a_coeff[Npade-1] * (omega - z[Npade-2]) + 1.;
		for (int p = Npade-2; p > 0; p--) C = 1./C * (omega - z[p-1]) * a_coeff[p] + 1.;
		C = 1./C * a_coeff[0];
		//cout << omega.imag() << "\t" << C.real() << "\t" << C.imag() << "\n";
		outpade << omega << "\t" << C.real() << "\t" << C.imag() << "\n";
		s_pade += h_pade * C.imag();
	}
	outpade << endl << endl << "# integral : " << s_pade << "\n";
  outpade.close();
	
	return;
}








