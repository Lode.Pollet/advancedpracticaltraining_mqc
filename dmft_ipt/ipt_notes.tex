\input{preamble.tex}
% 
\newcommand{\semester}{Summer term 2021}
\newcommand{\authors}{Lukas Rammelm\"uller, Lode Pollet}
\newcommand{\lva}{Advanced practical training}
\newcommand{\extype}{The metal-insulator transition with iterated perturbation theory (IPT) as solver for dynamical mean-field theory (DMFT) -- Der Metall-Isolator \"Ubergang mit iterierter St\"orungsrechnung als L\"oser f\"ur die dynamische Molekularfeldn\"aherung. }
\renewcommand{\subtitle}{online April 8.  Value : 2 credits}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{bm}
\usepackage{graphicx}
\usepackage{color}


%\input{preamble.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\maketitlesection

\vspace{0.5cm}
%\section*{The metal-insulator transition with iterated perturbation theory (IPT) as solver for dynamical mean-field theory (DMFT)}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
The purpose of this project is to see the metal-insulator transition in the 2D square-lattice Hubbard model at half filling. Dynamical mean-field theory (DMFT) is arguably the simplest approximation able to see this transition and this partly explains its popularity. The idea behind DMFT is to map the full lattice problem to an effective impurity problem. Moreover, instead of seeking an exact solution for the impurity problem, we restrict ourselves to the iterated perturbation theory (IPT) in the formulation of Kajueter-Kotliar~\cite{Kajueter1996}, where the impurity self energy $\Sigma_{\text{imp}}$ is calculated to second order in perturbation theory and arbitrary densities can be taken. To simplify the analysis, we will use a Bethe lattice, where convergence is usually easier while the results vary little compared to the square lattice. Finally, we will use an analytic continuation method to get the spectral function for real frequencies.

\section{The Hubbard Model}
Consider the Hubbard model on a 2D square lattice
%
\begin{equation}
  H = - \frac{t}{\sqrt{z}}  \sum_{\langle i,j \rangle \sigma} c^{\dagger}_{i \sigma} c_{j \sigma} + U \sum_i (n_{i \uparrow} - \frac{1}{2} )( n_{i \downarrow} - \frac{1}{2} ). % - \mu \sum_{i \sigma} n_{i \sigma}.
\label{eq:HubbardHamiltonian}
\end{equation}
%
Writing the interaction term in the form shown in  Eq.~\ref{eq:HubbardHamiltonian} means that the chemical potential is $\mu=\frac{U}{2}$ and it makes the particle-hole symmetry at half filling manifest.
The coordination number $z=4$ on a 2D square lattice. %We will set $t=1/2$ as the energy unit.


\section{Dynamical mean-field theory (DMFT) in a nutshell}
This section briefly introduces the concept of DMFT and closely follows the appropriate sections of the review by Georges {\it et al.}~\cite{Georges1996} (see the paper for a more in-depth introduction). Also, the thesis by E. Gull might be helpful~\cite{GullDiss}.

As mentioned above, the objective of DMFT is to approximate the full lattice problem (e.g., the Hubbard model on a square lattice) by an impurity problem that feels an effective mean-field created by all other degrees of freedom. Of course this constitutes an approximation to the real problem, although as we shall see in the course of this project, a sufficient one to see relevant physical phenomena such as the metal-insulator transition. In the limit of inifinite coordination number $z\to\infty$ the mapping becomes exact - in this limit fluctuations of single neighbors are negligible. Therefore one expects the DMFT approach to work better for lattices with large coordination number.

The impurity is described by the action (in imaginary time)
%
\begin{eqnarray}
  S_{\text{imp}} & = & -\int_0^{\beta} \d\tau \int_0^{\beta} \d\tau^{\prime}\ \sum_{\sigma} \; c^{\dagger}_{0,\sigma}(\tau)\mathcal{G}_0^{-1}(\tau-\tau^{\prime})c_{0,\sigma}(\tau^{\prime}) \nonumber \\
  {} & + & U\int_0^{\beta} \d\tau \; \left( n_{0,\uparrow}(\tau) - \frac{1}{2}\right) \left(n_{0,\downarrow}(\tau) - \frac{1}{2}\right).
\end{eqnarray}
%
where $\mathcal{G}_0^{-1}(\tau-\tau^{\prime})$ plays the role of the \emph{dynamical} mean field (hence the name), that also takes into account \emph{local} quantum fluctuations on the impurity. Note that $\mathcal{G}_0$ is \emph{not} the non-interacting Green function of the original lattice Hamiltonian but that of the impurity.

To relate this effective field $\mathcal{G}_0$ to quantities computable from the effective action $S_{\rm imp}$ one introduces the \emph{self-consistency condition}
\begin{equation}
  \mathcal{G}_0(\i \omega_n)^{-1} = \i\omega_n+\mu+G(\i\omega_n)^{-1} - R[G(\i\omega_n)],
  \label{eq:self_consistency}
\end{equation}
where the Green function of the impurity is defined as

\begin{equation}
  G(\tau-\tau') \equiv -\langle\mathcal{T}c(\tau)c^\dagger(\tau')\rangle_{S_{\rm imp}}
\end{equation}
and
\begin{equation}
  G(\i\omega_n) = \int_0^{\beta}\d\tau\ G(\tau)\e^{\i\omega_n\tau}.
\end{equation}
By convention $\omega_n$ denote the fermionic Matsubara frequencies
\begin{equation}
  \omega_n = (2n+1)\frac{\pi}{\beta}.
\end{equation}
The information on the underlying lattice enters through the reciprocal function of the Hilbert transform. The latter is defined as
\begin{equation}
  \tilde{D}(\xi) = \int_{-\infty}^{+\infty}\d\epsilon\ \frac{D(\epsilon)}{\xi-\epsilon}
\end{equation}
where $D(\epsilon)$ denotes the density of states of the lattice of interest. The reciprocal function $R$ is defined via
\begin{equation}
  R[\tilde{D}(\xi)] = \xi.
\end{equation}

\begin{task}{Task I: Formalism}
  What is the density of states for a Bethe lattice (ie, a tree structure with infinite coordination number)? Use the above definition of the Hilbert transform to also find the corresponding reciprocal function $R[\tilde{D}(\xi)]$. \\

  Show that for a Bethe-lattice the selfconsistency relation \equref{self_consistency} simplifies to
  %
  \begin{equation}
    \mathcal{G}_0(\i\omega_n) = \frac{1}{\i \omega_n - t^2 G(\i \omega_n)}
  \end{equation}
  %
  \emph{Note:} In the IPT scheme, the Green functions $G_{\sigma}$ and $\mathcal{G}_{0\sigma}$ (with $\sigma = \uparrow, \downarrow$)  have a Hartree shift $\mu \to \mu - U n_{\sigma}$ originating from an infinite sum of tadpole diagrams. Note that we do not always write the spin label $\sigma$ for the Green functions since they are assumed to be the same for a paramagnetic bath.
\end{task}


The above equations describe a closed set of mean-field equations which we aim to solve self-consistently. The numerical implementation should follow (roughly) these steps:

\begin{enumerate}
  \item Initialize the program with a guess for the full Green function $G(\i\omega_n)$.
  \item Use the self-consistency relation \equref{self_consistency} to obtain an estimate for $\mathcal{G}_0(\i\omega_n)$.
  \item Compute the self-energy $\Sigma_{\rm imp}(\i\omega_n)$.
  \item Use the Dyson equation to obtain a new estimate for $G(\i\omega_n)$:
    \begin{equation}
      G(\i\omega_n) = \mathcal{G}_0(\i\omega_n)^{-1} - \Sigma_{\rm imp}(\i\omega_n).
    \end{equation}
  \item Go back to step (ii) to get a new $\mathcal{G}_0(\i\omega_n)$ and monitor the convergence of the iterative scheme by checking the change of $\mathcal{G}_0(\i\omega_n)$ as compared to the previous iteration.
\end{enumerate}

The convergence should be very rapid in the metallic phase, but may be more difficult near the Mott transition. In such cases, it may help to mix in the new components more slowly, i.e., $\mathcal{G}_0 = \alpha \mathcal{G}_0^{\rm new} + (1-\alpha)  \mathcal{G}_0^{\rm old}$. %Alternatively, better starting guesses may help.   %%% It should converge as is, so I commented it out (LP).\\


\begin{task}{Task II: Numerical implementation}
  Implement the above DMFT scheme to self-consistently find the impurity Green function $G$. The code must be submitted and needs to be well documented - explain your steps!\\

  To compute the self-energy use perturbation theory: Draw the  self energy diagram in second order for the impurity action in terms of the Green functions $\mathcal{G}_0(\tau)$ of the impurity problem. Solve it in imaginary time where its evaluation is easier, see Kajueter-Kotliar~\cite{Kajueter1996}.\\

  An important ingredient we need is the Fourier transform (and its inverse) between the imaginary-time representation of the Green function and the Matsubara Green function. One computes only Fourier transforms between $G(\tau) - g(\tau)$ and $G(\omega_n) - g(\omega_n)$, where $g$ is a function which has the same asymptotic behavior in Matsubara space as $G$ (such as the non-interacting Green function, which is known analytically in both representations).
\end{task}



\section{Analytic continuation}
Although the metal and insulator phases show qualitative differences already in $G(\tau)$ the interpretation is more transparent when looking at the spectral function
%
\begin{equation}
  A(\omega) = -2 \, {\rm Im} \, \lim_{\i\omega_n \to \omega + \i \delta}  G(\i\omega_n).
  \label{eq:spectralfun}
\end{equation}
%
Its computation requires however analytic continuation, which is a challenging task in general and several ways exist to attack the problem. The simplest way is to use Pad{\'e} approximants. Below the this method is briefly outlined following the scheme in~\cite{Vidberg1977}.

Pade approximants are given by a rational function $C_N(z)$ which approximates an arbitrary function $f(z)$ at some order $N$. Here we will use an approximation where $N$ is the number of Matsubara frequencies considered for the solution of $G(z_n = \i\omega_n)$ with the condition that the approximant
\begin{equation}
  C_N(z) = \frac{A_N(z)}{B_N(z)}
\end{equation}
is equal to the original function at the complex points $z_n$:
\begin{equation}
  C_N(z_n) = G(z_n) = f_n.
\end{equation}
The functions $A_N(z)$ and $B_N(z)$ are defined by the recursive relations
\begin{align}
  A_{n+1}(z) &= A_n(z) + (z-z_n)a_{n+1}A_{n-1}(z), \\
  B_{n+1}(z) &= B_n(z) + (z-z_n)a_{n+1}B_{n-1}(z),
\end{align}
with
\begin{equation}
  A_0 = 0, \qquad A_1 = a_a, \qquad B_0 = B_1 = 1.
\end{equation}
The coefficients $a_n$ are defined by the recursion
\begin{equation}
  g_p(z) = \frac{g_{p-1}(z_{p-1})-g_{p-1}(z)}{(z-z_{p-1})g_{p-1}(z)}
\end{equation}
for $p>1$ and
\begin{align}
  g_1(z_n) &= f_n, \\
  a_n &= g_n(z_n)
\end{align}
for $n=1\dots N$ such that $a_n$ is required for every Matsubara frequency $z_n=\i\omega_n$. This function can then be evaluated at real frequencies such that we obtain the desired result on the real line. Note that depending on the choice of parameters there could be instabilities with this procedure so that some tweaking with the parameters might be necessary.

As an alternative to the approach outlined above one could use a novel approach based on Nevanlinna theory~\cite{Fei2021}. It has the advantage of respecting the analytic properties of Green functions explicitly and is believed to yield superior results.
\vspace*{1cm}

\begin{figure}[t]
  \includegraphics[width=0.8\columnwidth]{spectrum_sketch.pdf}
  %\includegraphics[width=0.8\columnwidth]{LMU_bar.pdf}
  \centering
  \caption{Qualitative picture of the spectral function for small $U$ (left) and larger $U$ (right).}
  \label{fig:spectral_sketch}
\end{figure}

\begin{task}{Task III: Analytic continuation}
  Perform the analytic continuation by the method of your choice in order to compute the spectral function in Eq.\ref{eq:spectralfun}. \\

  You should find that for low values of $U$ the spectral function is close to the density of states of the Bethe lattice whereas for larger values of $U$ the Hubbard bands are formed. Qualitatively this trend is sketched in \figref{spectral_sketch}.
\end{task}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{unsrt}
\bibliography{ipt_refs}

\end{document}
