# AdvancedPracticalTraining_MQC

A collection of projects intended for the Advanced Practical Training of the Munich Quantum Center

Currently contains:
- Froehlich : diagrammatic Monte Carlo simulation of the Froehlich polaron at T=0. Based on the lecture notes in Scipost (Greitemann, Pollet) for the bare green function, selfenergy, and bold green function.
- DMFT : iterative perturbation theory solves for the dynamical mean-field theory (DMFT) study of the metal to insulator transition. Analytical continuation is performed by Pade approximants.
